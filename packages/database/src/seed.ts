import { Course, Ksat, LinkedResource, Module, Proficiency, Sequence, Source, Unit, VJQR, Workrole } from '@prisma/client';
import courses from '../nexttl_content/courses.json';
import ksats from '../nexttl_content/ksats.json';
import linkedResources from '../nexttl_content/linkedResources.json';
import modules from '../nexttl_content/modules.json';
import proficiencies from '../nexttl_content/proficiencies.json';
import sequences from '../nexttl_content/sequences.json';
import sources from '../nexttl_content/sources.json';
import units from '../nexttl_content/units.json';
import vjqrs from '../nexttl_content/vjqrs.json';
import workroles from '../nexttl_content/workroles.json';
import _assessments from '../nexttl_content/_assessments.json';
import _courseContent from '../nexttl_content/_courseContent.json';
import _ksatToModules from '../nexttl_content/_ksatToModules.json';
import _practice from '../nexttl_content/_practice.json';
import _resources from '../nexttl_content/_resources.json';
import { prisma } from '../prisma';

export const seed = async () => {
	// Note: must insert in an order that doesn't violate FK constraints
	await prisma.workrole.createMany({ data: workroles as Workrole[] });
	await prisma.source.createMany({ data: sources as Source[] });
	await prisma.ksat.createMany({ data: ksats as Ksat[] });
	await prisma.proficiency.createMany({ data: proficiencies as Proficiency[] });
	await prisma.sequence.createMany({ data: sequences as Sequence[] });
	await prisma.unit.createMany({ data: units as Unit[] });
	await prisma.module.createMany({ data: modules as Module[] });
	await prisma.linkedResource.createMany({ data: linkedResources as LinkedResource[] });
	await prisma.course.createMany({ data: courses as Course[] });
	await prisma.vJQR.createMany({ data: vjqrs as VJQR[] });

	for (const join of _ksatToModules) {
		await prisma.ksat.update({
			// @ts-ignore
			where: { id: join.A },
			data: {
				modulesThatUseIt: {
					// @ts-ignore
					connect: { id: join.B },
				},
			},
		});
	}

	for (const join of _assessments) {
		await prisma.linkedResource.update({
			// @ts-ignore
			where: { id: join.A },
			data: {
				modulesThatUseForAssessments: {
					// @ts-ignore
					connect: { id: join.B },
				},
			},
		});
	}

	for (const join of _courseContent) {
		await prisma.course.update({
			// @ts-ignore
			where: { id: join.A },
			data: {
				content: {
					// @ts-ignore
					connect: { id: join.B },
				},
			},
		});
	}

	for (const join of _practice) {
		await prisma.linkedResource.update({
			// @ts-ignore
			where: { id: join.A },
			data: {
				modulesThatUseForPractice: {
					// @ts-ignore
					connect: { id: join.B },
				},
			},
		});
	}

	for (const join of _resources) {
		await prisma.linkedResource.update({
			// @ts-ignore
			where: { id: join.A },
			data: {
				modulesThatUseForResources: {
					// @ts-ignore
					connect: { id: join.B },
				},
			},
		});
	}

	console.log(`🌱  The seed command has been executed.`);
};

if (require.main === module) {
	seed();
}
