import fs from 'fs/promises';
import { prisma } from '../prisma';

const dumpDir = './nexttl_content';

export const dump = async () => {
	const workroles = await prisma.workrole.findMany();
	await fs.writeFile(`${dumpDir}/workroles.json`, JSON.stringify(workroles, null, 2));

	const sources = await prisma.source.findMany();
	await fs.writeFile(`${dumpDir}/sources.json`, JSON.stringify(sources, null, 2));

	const ksats = await prisma.ksat.findMany();
	await fs.writeFile(`${dumpDir}/ksats.json`, JSON.stringify(ksats, null, 2));

	const proficiencies = await prisma.proficiency.findMany();
	await fs.writeFile(`${dumpDir}/proficiencies.json`, JSON.stringify(proficiencies, null, 2));

	const sequences = await prisma.sequence.findMany();
	await fs.writeFile(`${dumpDir}/sequences.json`, JSON.stringify(sequences, null, 2));

	const units = await prisma.unit.findMany();
	await fs.writeFile(`${dumpDir}/units.json`, JSON.stringify(units, null, 2));

	const modules = await prisma.module.findMany();
	await fs.writeFile(`${dumpDir}/modules.json`, JSON.stringify(modules, null, 2));

	const linkedResources = await prisma.linkedResource.findMany();
	await fs.writeFile(`${dumpDir}/linkedResources.json`, JSON.stringify(linkedResources, null, 2));

	const courses = await prisma.course.findMany();
	await fs.writeFile(`${dumpDir}/courses.json`, JSON.stringify(courses, null, 2));

	const vjqrs = await prisma.vJQR.findMany();
	await fs.writeFile(`${dumpDir}/vjqrs.json`, JSON.stringify(vjqrs, null, 2));

	const _ksatToModules = await prisma.$queryRaw`SELECT * FROM public."_KsatToModule"`;
	await fs.writeFile(`${dumpDir}/_ksatToModules.json`, JSON.stringify(_ksatToModules, null, 2));

	const _assessments = await prisma.$queryRaw`SELECT * FROM public."_assessments"`;
	await fs.writeFile(`${dumpDir}/_assessments.json`, JSON.stringify(_assessments, null, 2));

	const _courseContent = await prisma.$queryRaw`SELECT * FROM public."_courseContent"`;
	await fs.writeFile(`${dumpDir}/_courseContent.json`, JSON.stringify(_courseContent, null, 2));

	const _practice = await prisma.$queryRaw`SELECT * FROM public."_practice"`;
	await fs.writeFile(`${dumpDir}/_practice.json`, JSON.stringify(_practice, null, 2));

	const _resources = await prisma.$queryRaw`SELECT * FROM public."_resources"`;
	await fs.writeFile(`${dumpDir}/_resources.json`, JSON.stringify(_resources, null, 2));

	console.log(`💩  The dump command has been executed.`);
};

if (require.main === module) {
	dump();
}
