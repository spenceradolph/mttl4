import { prisma } from '../prisma';

export const deleteRows = async () => {
	await prisma.workrole.deleteMany();
	await prisma.source.deleteMany();
	await prisma.ksat.deleteMany();
	await prisma.proficiency.deleteMany();
	await prisma.sequence.deleteMany();
	await prisma.unit.deleteMany();
	await prisma.module.deleteMany();
	await prisma.linkedResource.deleteMany();
	await prisma.course.deleteMany();
	await prisma.vJQR.deleteMany();

	console.log(`💨  The delete command has been executed.`);
};

if (require.main === module) {
	deleteRows();
}
