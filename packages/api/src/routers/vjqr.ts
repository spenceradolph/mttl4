import { db } from '@nexttl/database';
import { z } from 'zod';
import { protectedProcedure, router } from '../trpc';

export const vjqrRouter = router({
	myVJQRs: protectedProcedure.query(async ({ ctx }) => {
		return await db.vjqrs.findMany({
			where: {
				menteeEmail: ctx.session.email,
			},
		});
	}),

	menteeVJQRs: protectedProcedure.query(async ({ ctx }) => {
		// Admins see all links, except their own links are already in myVJQRs
		if (ctx.session.isAdmin) return await db.vjqrs.findMany({ where: { NOT: { menteeEmail: ctx.session.email } } });

		return await db.vjqrs.findMany({
			where: {
				mentorEmails: {
					has: ctx.session.email,
				},
			},
		});
	}),

	create: protectedProcedure.input(z.object({ vjqrInfo: z.any() })).mutation(async ({ input }) => {
		const { vjqrInfo } = input;

		// TODO: create vjqr via gitlab api

		// create '(sub)group' under 90cos/VJQR/DOT/<rank + name> if not exists

		// create '(sub)group' under 90cos/VJQR/DOT/<rank + name>/workrole_or_sequence_name?

		// create a commit with initial files

		// add memberships?

		// TODO: figure out all that goes into this (cross reference existing projects and current scripts)

		// get the `sequence`
		// for each unit in the sequence
		// 		create 'epic' (in the group)
		// 		for each module in the unit
		// 			create 'issue' that falls under that epic
		// 			contains
		// 				module name
		// 				description
		// 				objectives (as checkboxes)
		// 				assessments
		// 				practice
		// 				etc
	}),
});
