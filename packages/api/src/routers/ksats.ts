import { db } from '@nexttl/database';
import { Ksat } from '@prisma/client'; // TODO: consider marking this as a dependency somehow?
import { z } from 'zod';
import { adminProcedure, protectedProcedure, router, schemaForType } from '../trpc';

const KsatSchema = schemaForType<Ksat>()(
	z.object({
		id: z.number(),
		ksatId: z.string(),
		type: z.enum(['Knowledge', 'Skill', 'Ability', 'Task']),
		jct_cs: z.string().nullable(),
		dcwf: z.string().nullable(),
		acc: z.string().nullable(),
		description: z.string(),
		comments: z.string().nullable(),
		sourceId: z.number().nullable(),
	})
);

export const ksatRouter = router({
	all: protectedProcedure.query(async () => await db.ksats.findMany()),

	update: adminProcedure.input(z.object({ storeKsats: z.array(KsatSchema) })).mutation(async ({ input, ctx }) => {
		const { storeKsats } = input;
		for (const storeKsat of storeKsats) {
			await db.ksats.update({
				where: { id: storeKsat.id },
				data: storeKsat,
			});
		}

		await db.commit({ msg: `KSAT updates made by ${ctx.session.username}` });
	}),
});
