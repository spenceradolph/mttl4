import { initTRPC, TRPCError } from '@trpc/server';
import superjson from 'superjson';
import { z } from 'zod';
import { Context } from './context';

export const t = initTRPC<{ ctx: Context }>()({
	transformer: superjson,
	errorFormatter({ shape }) {
		return shape;
	},
});

// exported for convenience
export const router = t.router;
const procedure = t.procedure;
const middleware = t.middleware;

// reusable procedures / middleware
export const protectedProcedure = procedure.use(
	middleware(async ({ ctx, next }) => {
		const { req, res, session } = ctx;
		if (!session) throw new TRPCError({ code: 'UNAUTHORIZED' });
		return next({ ctx: { session, req, res } });
	})
);

export const adminProcedure = procedure.use(
	middleware(async ({ ctx, next }) => {
		const { req, res, session } = ctx;
		if (!session || !session.isAdmin) throw new TRPCError({ code: 'UNAUTHORIZED' });
		return next({ ctx: { session, req, res } });
	})
);

// https://github.com/colinhacks/zod/issues/53
// https://github.com/colinhacks/zod/issues/372#issuecomment-826380330
// This function provides typesafety in generating a zod schema for a pre-existing type
export const schemaForType =
	<T>() =>
	<S extends z.ZodType<T, any, any>>(arg: S) => {
		return arg;
	};
