// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
	title: 'Nexttl',
	tagline: 'MTTL But Better',
	url: 'https://gitlab.com',
	baseUrl: '/docs/',
	onBrokenLinks: 'warn',
	onBrokenMarkdownLinks: 'warn',
	favicon: 'img/favicon.ico',

	plugins: [
		[
			require.resolve('@cmfcmf/docusaurus-search-local'),
			{
				indexBlog: false,
			},
		],
	],

	presets: [
		[
			'classic',
			/** @type {import('@docusaurus/preset-classic').Options} */
			({
				docs: {
					remarkPlugins: [require('mdx-mermaid')],
					sidebarPath: require.resolve('./sidebars.js'),
					routeBasePath: '/',
					// Please change this to your repo.
					// Remove this to remove the "edit this page" links.
					editUrl: 'https://gitlab.com/spenceradolph/nexttl_app/-/edit/main/packages/documentation',
				},
				blog: false,
				theme: {
					customCss: require.resolve('./src/css/custom.css'),
				},
			}),
		],
	],

	themeConfig:
		/** @type {import('@docusaurus/preset-classic').ThemeConfig} */
		({
			navbar: {
				// title: 'NexttlB',

				// logo: {
				// 	alt: 'My Site Logo',
				// 	src: 'img/logo.svg',
				// },
				items: [
					{
						label: 'Training',
						to: '../',
						target: '_parent',
					},
					{
						type: 'doc',
						docId: 'overview',
						position: 'left',
						label: 'Documentation',
					},
					{
						href: 'https://gitlab.com/spenceradolph/nexttl_app',
						label: 'GitLab',
						position: 'right',
					},
				],
			},
			footer: {
				style: 'dark',
				links: [
					{
						title: 'Docs',
						items: [
							{
								label: 'Tutorial',
								to: '/docs/overview',
							},
						],
					},
					{
						title: 'More',
						items: [
							{
								label: 'GitLab',
								href: 'https://gitlab.com/spenceradolph/nexttl_app',
							},
						],
					},
				],
				copyright: `Copyright © ${new Date().getFullYear()} Spencer Adolph`,
			},
			prism: {
				theme: lightCodeTheme,
				darkTheme: darkCodeTheme,
			},
		}),
};

module.exports = config;
