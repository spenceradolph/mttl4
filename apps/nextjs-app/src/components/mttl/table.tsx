import { KeyboardArrowLeft, KeyboardArrowRight } from '@mui/icons-material';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import LastPageIcon from '@mui/icons-material/LastPage';
import { Box, Button, IconButton, Modal, Table, TableBody, TableCell, TableFooter, TableHead, TablePagination, TableRow, TextField, Typography, useTheme } from '@mui/material';
import { createColumnHelper, flexRender, getCoreRowModel, getPaginationRowModel, useReactTable } from '@tanstack/react-table';
import { FC, useState } from 'react';
import { KsatRow, useStore } from '../../utils';

const modalStyle = {
	position: 'absolute' as 'absolute',
	top: '50%',
	left: '50%',
	transform: 'translate(-50%, -50%)',
	width: 400,
	bgcolor: 'background.paper',
	border: '2px solid #000',
	boxShadow: 24,
	p: 4,
};

interface TablePaginationActionsProps {
	count: number;
	page: number;
	rowsPerPage: number;
	onPageChange: (event: React.MouseEvent<HTMLButtonElement>, newPage: number) => void;
}

const TablePaginationActions = (props: TablePaginationActionsProps) => {
	const theme = useTheme();
	const { count, page, rowsPerPage, onPageChange } = props;

	const handleFirstPageButtonClick = (e: React.MouseEvent<HTMLButtonElement>) => {
		onPageChange(e, 0);
	};

	const handleBackButtonClick = (e: React.MouseEvent<HTMLButtonElement>) => {
		onPageChange(e, page - 1);
	};

	const handleNextButtonClick = (e: React.MouseEvent<HTMLButtonElement>) => {
		onPageChange(e, page + 1);
	};

	const handleLastPageButtonClick = (e: React.MouseEvent<HTMLButtonElement>) => {
		onPageChange(e, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
	};

	return (
		<Box sx={{ flexShrink: 0, ml: 2.5 }}>
			<IconButton onClick={handleFirstPageButtonClick} disabled={page === 0} aria-label="first page">
				{theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
			</IconButton>
			<IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
				{theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
			</IconButton>
			<IconButton onClick={handleNextButtonClick} disabled={page >= Math.ceil(count / rowsPerPage) - 1} aria-label="next page">
				{theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
			</IconButton>
			<IconButton onClick={handleLastPageButtonClick} disabled={page >= Math.ceil(count / rowsPerPage) - 1} aria-label="last page">
				{theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
			</IconButton>
		</Box>
	);
};

interface MttlTableProps {
	tableKsats: KsatRow[];
}

export const MttlTable: FC<MttlTableProps> = ({ tableKsats }) => {
	const [storeKsats, setStoreKsats] = useStore(({ ksats, setKsats }) => [ksats, setKsats]);
	const [editingKsat, setEditingKsat] = useState<KsatRow | null>(null);

	const confirmEdit = () => {
		if (!editingKsat) return;

		// add to store if not already in
		if (!editingKsat.isLocalEdit) {
			setStoreKsats([...storeKsats, { ...editingKsat, isLocalEdit: true }]);
			return;
		}

		// edit in store
		const indexOfEditedKsat = storeKsats.findIndex(({ id }) => id === editingKsat.id);
		storeKsats[indexOfEditedKsat] = editingKsat;
		setStoreKsats(storeKsats);
	};

	const columnHelper = createColumnHelper<KsatRow>();
	const columns = [
		columnHelper.display({
			id: 'actions',
			header: '',
			cell: ({ row: { original } }) => {
				const revertButton = !original.isLocalEdit ? null : <button onClick={() => setStoreKsats(storeKsats.filter(({ id }) => id !== original.id))}>❌</button>;
				return (
					<div style={{ margin: 0, padding: 0, width: 0, display: 'flex', flexDirection: 'row' }}>
						{revertButton}
						<button
							onClick={() => {
								setEditingKsat(original);
							}}
						>
							✏️
						</button>
					</div>
				);
			},
		}),
		columnHelper.accessor('ksatId', {
			header: 'KSAT-ID',
			cell: ({ row: { original } }) => {
				return <span>{original.ksatId}</span>;
			},
		}),
		columnHelper.accessor('description', {
			header: 'Description',
			cell: ({ row: { original } }) => {
				return <span>{original.description}</span>;
			},
		}),
	];

	const table = useReactTable({ data: tableKsats, columns, getCoreRowModel: getCoreRowModel(), getPaginationRowModel: getPaginationRowModel() });

	return (
		<Table>
			<TableHead>
				<TableRow>
					{table.getFlatHeaders().map((header) => (
						<TableCell key={header.id}>{`${header.column.columnDef.header}`}</TableCell>
					))}
				</TableRow>
			</TableHead>
			<TableBody>
				{table.getRowModel().rows.map((row) => (
					<TableRow key={row.id} sx={{ outline: row.original.isLocalEdit ? 'solid #D3E372 2px' : '' }}>
						{row.getVisibleCells().map((cell) => (
							<TableCell key={cell.id}>{flexRender(cell.column.columnDef.cell, cell.getContext())}</TableCell>
						))}
					</TableRow>
				))}
			</TableBody>
			<TableFooter>
				<TableRow>
					<TablePagination
						rowsPerPageOptions={[5, 10, 25, { label: 'All', value: table.getCoreRowModel().rows.length }]}
						colSpan={3}
						count={table.getCoreRowModel().rows.length}
						rowsPerPage={table.getState().pagination.pageSize}
						page={table.getState().pagination.pageIndex}
						onPageChange={(e, newPage) => table.setPageIndex(newPage)}
						onRowsPerPageChange={(e) => {
							table.setPageSize(parseInt(e.target.value, 10));
							table.setPageIndex(0);
						}}
						ActionsComponent={TablePaginationActions}
					/>
				</TableRow>
			</TableFooter>
			{editingKsat && (
				<Modal open={true} onClose={() => setEditingKsat(null)}>
					<Box sx={modalStyle}>
						<Typography>Edit {editingKsat.ksatId}</Typography>
						<TextField sx={{ mt: 2 }} value={editingKsat.description} onChange={(e) => setEditingKsat({ ...editingKsat, description: e.currentTarget.value })} />
						<Button onClick={() => confirmEdit()}>✅</Button>
					</Box>
				</Modal>
			)}
		</Table>
	);
};
