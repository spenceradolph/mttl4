import { VJQR } from '@prisma/client';
import { FC } from 'react';

interface VjqrListProps {
	vjqrs: VJQR[];
}

export const VjqrList: FC<VjqrListProps> = ({ vjqrs }) => {
	return (
		<table>
			<thead>
				<tr>
					<th>ID</th>
					<th>Link</th>
					<th>Mentee Email</th>
					<th>Mentor Email(s)</th>
				</tr>
			</thead>
			<tbody>
				{vjqrs.map(({ id, gitlabEpicLink, menteeEmail, mentorEmails }) => {
					return (
						<tr key={id}>
							<td>{id}</td>
							<td>
								<a href={gitlabEpicLink}>{gitlabEpicLink}</a>
							</td>
							<td>{menteeEmail}</td>
							<td>{mentorEmails}</td>
						</tr>
					);
				})}
			</tbody>
		</table>
	);
};
