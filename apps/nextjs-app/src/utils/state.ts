import { Ksat } from '@prisma/client'; // TODO: consider marking this as a dependency somehow?
import { useEffect, useState } from 'react';
import create from 'zustand';
import { devtools, persist } from 'zustand/middleware';

export type KsatRow = Ksat & {
	isLocalEdit: boolean;
};

interface AppState {
	ksats: KsatRow[];
	setKsats: (ksats: AppState['ksats']) => void;
}

const emptyState: AppState = {
	ksats: [],
	setKsats: (ksats) => {},
};

const usePersistedStore = create<AppState>()(
	devtools(
		persist(
			(set) => ({
				ksats: [],
				setKsats: (ksats: AppState['ksats']) => set({ ksats }),
			}),
			{ name: 'nexttl-local-storage' }
		)
	)
);

// https://github.com/pmndrs/zustand/issues/1145
// This a fix to ensure zustand never hydrates the store before React hydrates the page
// else it causes a mismatch between SSR/SSG and client side on first draw which produces an error

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// Terms and conditions for copying: 1 beer for Spencer
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

export const useStore = ((selector, compare) => {
	const store = usePersistedStore(selector, compare);
	const [hydrated, setHydrated] = useState(false);
	useEffect(() => setHydrated(true), []);
	return hydrated ? store : selector(emptyState);
}) as typeof usePersistedStore;
