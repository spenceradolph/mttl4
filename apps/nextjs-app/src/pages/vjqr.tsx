import type { NextPage } from 'next';
import { useState } from 'react';
import { VjqrList } from '../components';
import { api } from '../utils';

const VJQR: NextPage = () => {
	const { data: myLinks } = api.vjqrs.myVJQRs.useQuery();
	const { data: menteeLinks } = api.vjqrs.menteeVJQRs.useQuery();
	const [selectedTab, setSelectedTab] = useState<'MyLinks' | 'MenteeLinks'>('MyLinks');

	if (!myLinks || !menteeLinks) return <div>Loading...</div>;

	return (
		<>
			<h1>VJQRs</h1>
			<button onClick={() => setSelectedTab('MyLinks')} style={{ outline: selectedTab === 'MyLinks' ? 'solid green 2px' : '' }}>
				View My Links
			</button>
			<button onClick={() => setSelectedTab('MenteeLinks')} style={{ outline: selectedTab === 'MenteeLinks' ? 'solid green 2px' : '' }}>
				View Mentee Links
			</button>
			<div style={{ outline: 'solid black 1px' }}>{selectedTab === 'MyLinks' ? <VjqrList vjqrs={myLinks} /> : <VjqrList vjqrs={menteeLinks} />}</div>
		</>
	);
};

export default VJQR;
