/** @type {import('next').NextConfig} */
const nextConfig = {
	reactStrictMode: true,
	swcMinify: true,
	// Containers Have Trouble with Hot-Reload
	webpack: (config) => {
		config.watchOptions = {
			poll: 1000,
			aggregateTimeout: 300,
		};
		return config;
	},

	experimental: {
		// Experimental monorepo support
		// @link {https://github.com/vercel/next.js/pull/22867|Original PR}
		// @link {https://github.com/vercel/next.js/discussions/26420|Discussion}
		externalDir: true,
		images: {
			allowFutureImage: true,
		},
	},

	// Docusaurus Static Site is an SPA
	rewrites: async () => {
		return [
			{
				source: '/docs/:customPage*',
				destination: '/docs/:customPage*/index.html',
			},
		];
	},
};

module.exports = nextConfig;
